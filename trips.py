from xlrd import open_workbook
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
from matplotlib import pylab
from math import sqrt
import numpy as np
import os


def load_data(car_list, filter=None):
    cars = {} 
    for car in car_list:
        wb = open_workbook(car, logfile=open(os.devnull, 'w'))
        trips = []
        cars[car] = trips
        for s in wb.sheets():
            if s.name.startswith("Trip"):   
                trip = None
                for row in range(s.nrows):
                    first = s.cell(row, 2).value
                    if not isinstance(first, float):
                        continue
                    location = (s.cell(row, 9).value).encode('ascii', 'ignore')
                    if filter is None or filter in location:
                        if trip is None:
                            trip = []
                            trips.append(trip)
                        time = s.cell(row, 1).value * 24 * 3600
                        latitude, longitude = s.cell(row, 2).value, s.cell(row, 3).value
                        speed = s.cell(row, 4).value
                        lin_g, lat_g = s.cell(row, 7).value, s.cell(row, 8).value
                        road_speed = s.cell(row, 0).value == "RD. Speed"
                        turning = s.cell(row, 0).value == "Turning"
                        trip.append([time, latitude, longitude, speed, lin_g, lat_g, road_speed, turning])
    return cars


def dist(x, y):
    return sqrt((x[0] - y[0]) ** 2 + (x[1] - y[1]) ** 2)

def latlon_distance(lat1, long1, lat2, long2):
    # Convert latitude and longitude to 
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0
    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians
    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians
    # Compute spherical distance from spherical coordinates.
    # For two locations in spherical coordinates 
    # (1, theta, phi) and (1, theta', phi')
    # cosine( arc length ) = 
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length
    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) + 
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )
    # Multiply arc by the radius of the earth to get answer in meters
    return arc * 6.3674447E6
    
if __name__ == "__main__":
    cars = load_data(['data/BC41HLGP.xls', 'data/BC78HLGP.xls'], filter="Southern")
    # cars = load_data(['data/wynand.xls'])
    # map = Basemap(llcrnrlon=10.,llcrnrlat=-37.,urcrnrlon=40.,urcrnrlat=-18.,
    #             projection='lcc',lat_1=-20.,lat_2=-40.,lon_0=15.,
    #             resolution ='l',area_thresh=1000.)
    if 1:
        im = plt.imread('data/background_gauteng.png')
        map = Basemap(llcrnrlon=27.9381,llcrnrlat=-26.2840,urcrnrlon=28.1950,urcrnrlat=-26.1437,
                    resolution='h', projection='merc')
        map.imshow(im, interpolation='lanczos', origin='upper')
        map2 = Basemap(llcrnrlon=27.9381,llcrnrlat=-26.2840,urcrnrlon=28.1950,urcrnrlat=-26.1437,
                    resolution='h', projection='merc')
        plt.figure(6)
        map2.imshow(im, interpolation='lanczos', origin='upper')
        map2.llcrnrlon = 27.9586
        map2.urcrnrlon = 28.0228
        map2.urcrnrlat = -26.2410
        map2.llcrnrlat = -26.2770
        map2.boundarylonmin = 27.9586
        map2.boundarylonmax = 28.0228
        ax = plt.gca()
        x0, y0 = map(27.9586, -26.2770)
        x1, y1 = map(28.0228, -26.2410)
        ax.set_ylim([y0, y1])
        ax.set_xlim([x0, x1])
        plt.figure(1)
        anchor = [27.954517, -26.264238]
    else:
        im = plt.imread('data/background_cape.png')
        map = Basemap(llcrnrlon=18.4316,llcrnrlat=-34.0413,urcrnrlon=18.8951,urcrnrlat=-33.7632,
                    resolution='h', projection='merc')
        map.imshow(im, interpolation='lanczos', origin='upper')
        map2 = Basemap(llcrnrlon=18.4316,llcrnrlat=-34.0413,urcrnrlon=18.8951,urcrnrlat=-33.7632,
                    resolution='h', projection='merc')
        map2.imshow(im, interpolation='lanczos', origin='upper')

        anchor = None
    colors = [(0, 0.5, 1.0), (1.0, 0.1, 0)]
    shape = ["o", "."]

    if anchor:
        lat, lon = anchor
        x, y = map(lat, lon)
        map.plot(x, y, 'gd', markersize=20)

    speeds = [] 
    accels_lin = []
    accels_lon = []
    lat_diffs = []
    lon_diffs = []
    times = []
    for i, car in enumerate(cars.items()):
        car_name, trips = car
        speeds.append([])
        accels_lin.append([])
        accels_lon.append([])
        lon_diffs.append([])
        lat_diffs.append([])
        times.append([])
        for trip in trips:
            if anchor and dist(anchor, trip[0]) > dist(anchor, trip[-1]):
                continue
            lats = []
            lons = []
            times[-1].append([])
            for item in trip:
                lat, lon = item[1], item[2]
                speeds[-1].append(item[3])
                accels_lin[-1].append(item[4])
                accels_lon[-1].append(item[5])
                times[-1][-1].append(item[0])
                lats.append(lat)
                lons.append(lon)
                x, y = map(lat, lon)
                # x, y = map(lon, lat)
                map.plot(x, y, 'o', color=colors[i], markersize=(len(cars) - i) * 5)
            lon_diffs[-1].extend(list(np.diff(np.array(lats))))
            lat_diffs[-1].extend(list(np.diff(np.array(lons))))

        # item = trips[0][0]
        # print item[1:3]
        # lat, lon = item[1], item[2]
    plt.figure(2)
    for i in range(len(cars)):
        if i == 0:
            plt.title("Histogram of speed on Southern Bypass drive")
        plt.subplot(2, 1, i + 1)
        plt.hist(speeds[i], bins=60, range=(0, 140))

    plt.figure(3)

    for i in range(len(cars)):
        plt.subplot(2, 2, i + 1)
        plt.hist(accels_lon[i], bins=60)
        plt.xlim([0, 0.8])
        plt.subplot(2, 2, i + 3)
        plt.hist(accels_lin[i], bins=60)
        plt.xlim([-0.3, 0.3])

    for i, trips in enumerate(cars.values()):
        plt.figure(5)
        plt.subplot(2, 1, i + 1)
        diffs = []
        for trip in trips: 
            trip_times = np.array([item[0] for item in trip])
            diff = np.diff(trip_times)
            diffs.extend(list(diff))
            short_count = np.sum(diff < 2.0)
            if short_count > 3: 
                plt.plot(trip_times - trip_times[0], '.-')
                lin_accels = np.array([item[4] for item in trip])
                lat_accels = np.array([item[5] for item in trip])
                plt.plot(lin_accels)
                plt.plot(lat_accels)
                for item in trip:
                    lat, lon = item[1], item[2]
                    x, y = map2(lat, lon)
                    plt.figure(6)
                    map2.plot(x, y, 'o', color=colors[i], markersize=10)
        plt.figure(7)
        plt.subplot(2, 1, i + 1)
        plt.hist(diffs, bins=100, range=(0, 100))
        plt.figure(5)
    plt.show()
